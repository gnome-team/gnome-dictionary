# Kazakh translation for gnome-dictionary.
# Copyright (C) 2014 gnome-dictionary's COPYRIGHT HOLDER
# This file is distributed under the same license as the gnome-dictionary package.
# Baurzhan Muftakhidinov <baurthefirst@gmail.com>, 2014-2017.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-dictionary master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-dictionary/"
"issues\n"
"POT-Creation-Date: 2020-12-10 15:51+0000\n"
"PO-Revision-Date: 2021-03-01 19:30+0500\n"
"Last-Translator: Baurzhan Muftakhidinov <baurthefirst@gmail.com>\n"
"Language-Team: Kazakh <kk_KZ@googlegroups.com>\n"
"Language: kk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Poedit 2.4.2\n"

#: data/appdata/org.gnome.Dictionary.appdata.xml.in.in:6
msgid "GNOME Dictionary"
msgstr "GNOME сөздігі"

#: data/appdata/org.gnome.Dictionary.appdata.xml.in.in:7
#: data/org.gnome.Dictionary.desktop.in.in:4
msgid "Check word definitions and spellings in an online dictionary"
msgstr "Сөз анықтамасын және емлесін желідегі сөздікте тексеру"

#: data/appdata/org.gnome.Dictionary.appdata.xml.in.in:9
msgid ""
"GNOME Dictionary is a simple dictionary application that looks up "
"definitions of words online. Though it looks up English definitions by "
"default, you can easily switch to Spanish or add other online dictionaries "
"using the DICT protocol to suit your needs."
msgstr ""
"GNOME сөздігі - бұл сөздер анықтамаларын желідегі сөздіктен іздейтін "
"қарапайым қолданба. Ол бастапқыда ағылшын тілінде іздеседе, сіз оңай түрде "
"испан тіліне ауысып, немесе керек болса DICT хаттамасын қолданатын басқа да "
"желідегі сөздіктерді қоса аласыз."

#: data/appdata/org.gnome.Dictionary.appdata.xml.in.in:34
msgid "The GNOME Project"
msgstr "GNOME жобасы"

#: data/default.desktop.in:5
msgid "Default Dictionary Server"
msgstr "Үнсіз келісім бойынша сөздіктер сервері"

#: data/org.gnome.Dictionary.desktop.in.in:3 src/gdict-about.c:62
#: src/gdict-app.c:369 src/gdict-window.c:495 src/gdict-window.c:1511
msgid "Dictionary"
msgstr "Сөздік"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.Dictionary.desktop.in.in:6
msgid "word;synonym;definition;spelling;"
msgstr "word;synonym;definition;spelling;сөз;синоним;анықтама;емле;"

#: data/org.gnome.dictionary.gschema.xml:6
msgid "The default database to use"
msgstr "Қолдану үшін бастапқы дерекқор"

#: data/org.gnome.dictionary.gschema.xml:7
msgid ""
"The name of the default individual database or meta-database to use on a "
"dictionary source. An exclamation mark (“!”) means that all the databases "
"present in a dictionary source should be searched"
msgstr ""
"Сөздіктерді ұсынатын қайнар көзінде үнсіз келісім бойынша қолданылатын бөлек "
"дерекқор немесе метадерекқор аты. Леп белгісі (\"!\") қойылса, онда іздеу "
"қайнар көздегі барлық сөздіктер ішінде орындалады"

#: data/org.gnome.dictionary.gschema.xml:11
msgid "The default search strategy to use"
msgstr "Қолданылатын үнсіз келісім стратегиясы"

#: data/org.gnome.dictionary.gschema.xml:12
msgid ""
"The name of the default search strategy to use on a dictionary source, if "
"available. The default strategy is “exact”, that is match exact words."
msgstr ""
"Сөздіктерді ұсынатын қайнар көзінде қолжетерлік болса, үнсіз келісім бойынша "
"қолданылатын стратегия. Бастапқы стратегия - \"exact\", яғни, тек толық "
"сөздерді сәйкестендіру."

#: data/org.gnome.dictionary.gschema.xml:16
msgid "The font to be used when printing"
msgstr "Баспаға шығару үшін қолданылатын қаріп"

#: data/org.gnome.dictionary.gschema.xml:17
msgid "The font to be used when printing a definition."
msgstr "Анықтамасын шығару үшін қолданылатын қаріп."

#: data/org.gnome.dictionary.gschema.xml:21
msgid "The name of the dictionary source used"
msgstr "Сөздіктерді ұсынатын қайнар көзінің аты"

#: data/org.gnome.dictionary.gschema.xml:22
msgid ""
"The name of the dictionary source used to retrieve the definitions of words."
msgstr ""
"Сөздер анықтамаларын алуға қолданылған сөздіктерді ұсынатын қайнар көзінің "
"аты."

#. Translators: Do not translate the Name key
#: data/thai.desktop.in:4
msgid "Thai"
msgstr "Thai"

#: data/thai.desktop.in:5
msgid "Longdo Thai-English Dictionaries"
msgstr "Longdo Тай-Ағылшын сөздіктері"

#: src/gdict-about.c:52
msgid "translator-credits"
msgstr "Baurzhan Muftakhidinov <baurthefirst@gmail.com>"

#: src/gdict-about.c:54
msgid "Look up words in dictionaries"
msgstr "Сөздерді сөздіктерден іздеу"

#: src/gdict-about.c:61
msgid "Dictionary (development build)"
msgstr "Сөздік (өндірілетін нұсқасы)"

#: src/gdict-app-menus.ui:5
msgid "_New Window"
msgstr "_Жаңа терезе"

#: src/gdict-app-menus.ui:12
msgid "_Save a Copy…"
msgstr "Көшірмесін _сақтау…"

#: src/gdict-app-menus.ui:18
msgid "P_review"
msgstr "Ал_дын-ала қарау"

#: src/gdict-app-menus.ui:23
msgid "_Print"
msgstr "Бас_паға шығару"

#: src/gdict-app-menus.ui:30
msgid "_Find"
msgstr "_Табу"

#: src/gdict-app-menus.ui:37
msgid "_View"
msgstr "Тү_рі"

#: src/gdict-app-menus.ui:40
msgid "_Sidebar"
msgstr "_Бүйір панелі"

#: src/gdict-app-menus.ui:47
msgid "Similar _Words"
msgstr "Ұқсас _сөздер"

#: src/gdict-app-menus.ui:52
msgid "Dictionary Sources"
msgstr "Сөздіктер қайнар көздері"

#: src/gdict-app-menus.ui:57
msgid "Available St_rategies"
msgstr "Қолжетерлік с_тратегиялар"

#: src/gdict-app-menus.ui:64
msgid "_Go"
msgstr "Ө_ту"

#: src/gdict-app-menus.ui:67
msgid "_Previous Definition"
msgstr "_Алдыңғы анықтама"

#: src/gdict-app-menus.ui:72
msgid "_Next Definition"
msgstr "_Келесі анықтама"

#: src/gdict-app-menus.ui:79
msgid "_First Definition"
msgstr "_Бірінші анықтама"

#: src/gdict-app-menus.ui:84
msgid "_Last Definition"
msgstr "_Соңғы анықтама"

#: src/gdict-app-menus.ui:93
msgid "Pr_eferences"
msgstr "Ба_птаулар"

#: src/gdict-app-menus.ui:97
msgid "_Keyboard Shortcuts"
msgstr "П_ернетақта жарлықтары"

#. The help button is always visible
#: src/gdict-app-menus.ui:101 src/gdict-source-dialog.c:666
msgid "_Help"
msgstr "_Көмек"

#: src/gdict-app-menus.ui:106
msgid "_About Dictionary"
msgstr "Сөздік қолданбасы тур_алы"

#: src/gdict-app.c:45 src/gdict-app.c:74
msgid "Words to look up"
msgstr "Ізделетін сөздер"

#: src/gdict-app.c:45 src/gdict-app.c:51
msgid "WORD"
msgstr "СӨЗ"

#: src/gdict-app.c:51
msgid "Words to match"
msgstr "Сәйкестіру үшін сөздер"

#: src/gdict-app.c:57
msgid "Dictionary source to use"
msgstr "Қолданылатын сөздіктердің қайнар көзі"

#: src/gdict-app.c:57 src/gdict-app.c:63 src/gdict-app.c:69
msgid "NAME"
msgstr "АТЫ"

#: src/gdict-app.c:63
msgid "Database to use"
msgstr "Қолданылатын сөздік"

#: src/gdict-app.c:69
msgid "Strategy to use"
msgstr "Қолданылатын стратегия"

#: src/gdict-app.c:74
msgid "WORDS"
msgstr "СӨЗДЕР"

#: src/gdict-app.c:106
msgid "Dictionary Preferences"
msgstr "Сөздік баптаулары"

#: src/gdict-app.c:137 src/gdict-source-dialog.c:463
msgid "There was an error while displaying help"
msgstr "Көмекті көрсету кезінде қате кетті"

#: src/gdict-client-context.c:773
#, c-format
msgid "No connection to the dictionary server at “%s:%d”"
msgstr "\"%s:%d\" бойынша орналасқан сөздіктер серверіне байланыс жоқ"

#: src/gdict-client-context.c:1056
#, c-format
msgid "Lookup failed for hostname “%s”: no suitable resources found"
msgstr ""
"\"%s\" хост атын іздеу сәтсіз аяқталды: сәйкес келетін ресурстар табылмады"

#: src/gdict-client-context.c:1087
#, c-format
msgid "Lookup failed for host “%s”: %s"
msgstr "\"%s\" хостын іздеу сәтсіз аяқталды: %s"

#: src/gdict-client-context.c:1121
#, c-format
msgid "Lookup failed for host “%s”: host not found"
msgstr "\"%s\" хостын іздеу сәтсіз аяқталды: хост табылмады"

#: src/gdict-client-context.c:1173
#, c-format
msgid ""
"Unable to connect to the dictionary server at “%s:%d”. The server replied "
"with code %d (server down)"
msgstr ""
"\"%s:%d\" бойынша орналасқан сөздіктер серверіне байланысу мүмкін емес. "
"Сервер %d кодымен жауап берді (сервер өшіп тұр)"

#: src/gdict-client-context.c:1192
#, c-format
msgid ""
"Unable to parse the dictionary server reply\n"
": “%s”"
msgstr ""
"Сөздіктер серверінің жауабын талдау мүмкін емес\n"
": “%s”"

#: src/gdict-client-context.c:1221
#, c-format
msgid "No definitions found for “%s”"
msgstr "\"%s\" үшін анықтамасы табылмады"

#: src/gdict-client-context.c:1236
#, c-format
msgid "Invalid database “%s”"
msgstr "Жарамсыз дерекқор \"%s\""

#: src/gdict-client-context.c:1251
#, c-format
msgid "Invalid strategy “%s”"
msgstr "Жарамсыз стратегия \"%s\""

#: src/gdict-client-context.c:1266
#, c-format
msgid "Bad command “%s”"
msgstr "Қате команда \"%s\""

#: src/gdict-client-context.c:1281
#, c-format
msgid "Bad parameters for command “%s”"
msgstr "\"%s\" командасы үшін қате параметр"

#: src/gdict-client-context.c:1296
#, c-format
msgid "No databases found on dictionary server at “%s”"
msgstr "\"%s\" бойынша орналасқан сөздіктер серверінен дерекқорлар табылмады"

#: src/gdict-client-context.c:1311
#, c-format
msgid "No strategies found on dictionary server at “%s”"
msgstr "\"%s\" бойынша орналасқан сөздіктер серверінен стратегиялар табылмады"

#: src/gdict-client-context.c:1743
#, c-format
msgid "Connection failed to the dictionary server at %s:%d"
msgstr ""
"%s:%d бойынша орналасқан сөздіктер серверіне байланысты орнату сәтсіз "
"аяқталды"

#: src/gdict-client-context.c:1782
#, c-format
msgid ""
"Error while reading reply from server:\n"
"%s"
msgstr ""
"Серверден жауапты оқу қатемен аяқталды:\n"
"%s"

#: src/gdict-client-context.c:1855
#, c-format
msgid "Connection timeout for the dictionary server at “%s:%d”"
msgstr ""
"\"%s:%d\" бойынша орналасқан сөздіктер серверіне байланысты орнатуды күту "
"уақыты бітті"

#: src/gdict-client-context.c:1889
#, c-format
msgid "No hostname defined for the dictionary server"
msgstr "Сөздіктер сервері үшін хост аты берілмеген"

#: src/gdict-client-context.c:1925 src/gdict-client-context.c:1940
#, c-format
msgid "Unable to create socket"
msgstr "Сокетті жасау мүмкін емес"

#: src/gdict-client-context.c:1966
#, c-format
msgid "Unable to set the channel as non-blocking: %s"
msgstr "Арнаны блоктаушы емес ретінде орнату мүмкін емес: %s"

#: src/gdict-client-context.c:1981
#, c-format
msgid "Unable to connect to the dictionary server at “%s:%d”"
msgstr "\"%s:%d\" бойынша орналасқан сөздіктер серверіне байланысу мүмкін емес"

#: src/gdict-database-chooser.c:372
msgid "Reload the list of available databases"
msgstr "Қолжетерлік сөздіктер тізімін қайта жүктеу"

#: src/gdict-database-chooser.c:384
msgid "Clear the list of available databases"
msgstr "Қолжетерлік сөздіктер тізімін тазарту"

#: src/gdict-database-chooser.c:836 src/gdict-speller.c:766
#: src/gdict-strategy-chooser.c:773
msgid "Error while matching"
msgstr "Сәйкестендіру қатесі"

#: src/gdict-defbox.c:2388
msgid "Error while looking up definition"
msgstr "Анықтамасын іздеу қатесі"

#: src/gdict-defbox.c:2430 src/gdict-speller.c:724
msgid "Another search is in progress"
msgstr "Басқа іздеу жүруде"

#: src/gdict-defbox.c:2431 src/gdict-speller.c:725
msgid "Please wait until the current search ends."
msgstr "Ағымдағы іздеу аяқталғанша дейін күте тұрыңыз."

#: src/gdict-defbox.c:2470
msgid "Error while retrieving the definition"
msgstr "Анықтамасын алу қатесі"

#: src/gdict-help-overlay.ui:13
msgctxt "shortcut window"
msgid "General"
msgstr "Жалпы"

#: src/gdict-help-overlay.ui:18
msgctxt "shortcut window"
msgid "Show help"
msgstr "Көмекті көрсету"

#: src/gdict-help-overlay.ui:24
msgctxt "shortcut window"
msgid "Show keyboard shortcuts"
msgstr "Пернетақта жарлықтарын көрсету"

#: src/gdict-help-overlay.ui:32
msgctxt "shortcut window"
msgid "Open a new window"
msgstr "Жаңа терезені ашу"

#: src/gdict-help-overlay.ui:39
msgctxt "shortcut window"
msgid "Close current window"
msgstr "Ағымдағы терезені жабу"

#: src/gdict-help-overlay.ui:45
msgctxt "shortcut window"
msgid "Quit"
msgstr "Шығу"

#: src/gdict-help-overlay.ui:53
msgctxt "shortcut window"
msgid "Preferences"
msgstr "Баптаулар"

#: src/gdict-help-overlay.ui:60
msgctxt "shortcut window"
msgid "View sidebar"
msgstr "Бүйір панелін көрсету"

#: src/gdict-help-overlay.ui:68
msgctxt "shortcut window"
msgid "Dictionary definitions"
msgstr "Сөздік анықтамалары"

#: src/gdict-help-overlay.ui:73
msgctxt "shortcut window"
msgid "Similar words"
msgstr "Ұқсас сөздер"

#: src/gdict-help-overlay.ui:80
msgctxt "shortcut window"
msgid "Dictionary sources"
msgstr "Сөздіктердің қайнар көздері"

#: src/gdict-help-overlay.ui:87
msgctxt "shortcut window"
msgid "Available strategies"
msgstr "Қолжетерлік стратегиялар"

#: src/gdict-help-overlay.ui:95
msgctxt "shortcut window"
msgid "Navigate within a definition"
msgstr "Анықтама ішінде навигациялау"

#: src/gdict-help-overlay.ui:100
msgctxt "shortcut window"
msgid "Previous definition"
msgstr "Алдыңғы анықтама"

#: src/gdict-help-overlay.ui:107
msgctxt "shortcut window"
msgid "Next definition"
msgstr "Келесі анықтама"

#: src/gdict-help-overlay.ui:114
msgctxt "shortcut window"
msgid "First definition"
msgstr "Бірінші анықтама"

#: src/gdict-help-overlay.ui:121
msgctxt "shortcut window"
msgid "Last definition"
msgstr "Соңғы анықтама"

#: src/gdict-help-overlay.ui:129
msgctxt "shortcut window"
msgid "Others"
msgstr "Басқалар"

#: src/gdict-help-overlay.ui:134
msgctxt "shortcut window"
msgid "Search within a definition"
msgstr "Анықтама ішінде іздеу"

#: src/gdict-help-overlay.ui:141
msgctxt "shortcut window"
msgid "Find in text"
msgstr "Мәтіннен табу"

#: src/gdict-help-overlay.ui:148
msgctxt "shortcut window"
msgid "Save as copy"
msgstr "Көшірме ретінде сақтау"

#: src/gdict-help-overlay.ui:155
msgctxt "shortcut window"
msgid "Print preview"
msgstr "Баспа алдында алдын-ала қарау"

#: src/gdict-help-overlay.ui:162
msgctxt "shortcut window"
msgid "Print"
msgstr "Баспаға шығару"

#: src/gdict-pref-dialog.c:260
msgid "View Dictionary Source"
msgstr "Қолданылатын сөздіктердің қайнар көзін қарау"

#: src/gdict-pref-dialog.c:326
msgid "Add Dictionary Source"
msgstr "Қолданылатын сөздіктердің қайнар көзін қосу"

#: src/gdict-pref-dialog.c:370
#, c-format
msgid "Remove “%s”?"
msgstr "\"%s\" өшіру керек пе?"

#: src/gdict-pref-dialog.c:372
msgid "This will permanently remove the dictionary source from the list."
msgstr ""
"Бұл әрекет сөздіктерді ұсынатын қайнар көзін тізімнен толығымен өшіреді."

#: src/gdict-pref-dialog.c:375 src/gdict-source-dialog.c:575
#: src/gdict-window.c:819
msgid "_Cancel"
msgstr "Ба_с тарту"

#: src/gdict-pref-dialog.c:376
msgid "_Remove"
msgstr "Өші_ру"

#: src/gdict-pref-dialog.c:394
#, c-format
msgid "Unable to remove source “%s”"
msgstr "\"%s\" қайнар көзін өшіру мүмкін емес"

#: src/gdict-pref-dialog.c:441
msgid "Edit Dictionary Source"
msgstr "Қолданылатын сөздіктердің қайнар көзін түзету"

#: src/gdict-pref-dialog.ui:34
msgid "_Select a dictionary source for looking up words:"
msgstr "Сөздерді іздеу үшін сөздіктердің қайнар көзін _таңдаңыз:"

#: src/gdict-pref-dialog.ui:96
msgid "Add a new dictionary source"
msgstr "Сөздіктердің жаңа қайнар көзін қосу"

#: src/gdict-pref-dialog.ui:116
msgid "Remove the currently selected dictionary source"
msgstr "Ағымдағы таңдалып тұрған сөздіктердің қайнар көзін өшіру"

#: src/gdict-pref-dialog.ui:136
msgid "Edit"
msgstr "Түзету"

#: src/gdict-pref-dialog.ui:137
msgid "Edit the currently selected dictionary source"
msgstr "Ағымдағы таңдалып тұрған сөздіктердің қайнар көзін түзету"

#: src/gdict-pref-dialog.ui:157 src/gdict-source-dialog.ui:171
msgid "Source"
msgstr "Қайнар_көзі"

#: src/gdict-pref-dialog.ui:187
msgid "_Print font:"
msgstr "Бас_паға шығару қарібі:"

#: src/gdict-pref-dialog.ui:216
msgid "Set the font used for printing the definitions"
msgstr "Анықтамаларды баспаға шығару үшін қолданылатын қаріпті орнату"

#: src/gdict-pref-dialog.ui:241
msgid "Print"
msgstr "Баспаға шығару"

#: src/gdict-print.c:235 src/gdict-print.c:299
#, c-format
msgid "Unable to display the preview: %s"
msgstr "Алдын-ала қарауды көрсету мүмкін емес: %s"

#: src/gdict-source-chooser.c:278
msgid "Reload the list of available sources"
msgstr "Қолжетерлік қайнар көздер тізімін қайта жүктеу"

#: src/gdict-source-dialog.c:314 src/gdict-source-dialog.c:408
msgid "Unable to create a source file"
msgstr "Қайнар көзі файлын жасау мүмкін емес"

#: src/gdict-source-dialog.c:336 src/gdict-source-dialog.c:428
msgid "Unable to save source file"
msgstr "Қайнар көзі файлын сақтау мүмкін емес"

#. we just allow closing the dialog
#: src/gdict-source-dialog.c:571 src/gdict-source-dialog.c:585
msgid "_Close"
msgstr "_Жабу"

#: src/gdict-source-dialog.c:576
msgid "_Add"
msgstr "Қо_су"

#: src/gdict-source-dialog.c:584
msgid "C_ancel"
msgstr "Б_ас тарту"

#: src/gdict-source-dialog.ui:58
msgid "_Description"
msgstr "Ан_ықтамасы"

#: src/gdict-source-dialog.ui:73
msgid "_Port"
msgstr "_Порт"

#: src/gdict-source-dialog.ui:87
msgid "_Hostname"
msgstr "_Хост аты"

#: src/gdict-source-dialog.ui:101
msgid "_Transport"
msgstr "Кө_лік"

#: src/gdict-source-dialog.ui:115
msgid "2628"
msgstr "2628"

#: src/gdict-source-dialog.ui:127
msgid "dict.org"
msgstr "dict.org"

#: src/gdict-source-dialog.ui:139
msgid "Source Name"
msgstr "Қайнар көзінің аты"

#: src/gdict-source-dialog.ui:198
msgid "Dictionaries"
msgstr "Сөздіктер"

#: src/gdict-source-dialog.ui:223
msgid "Strategies"
msgstr "Стратегияларьф"

#: src/gdict-source.c:414
#, c-format
msgid "Invalid transport type “%d”"
msgstr "\"%d\" көлік түрі қате"

#: src/gdict-source.c:442
#, c-format
msgid "No “%s” group found inside the dictionary source definition"
msgstr "Сөздікті ұсынатын қайнар көзінің анықтамасында \"%s\" тобы табылмады"

#: src/gdict-source.c:458 src/gdict-source.c:491 src/gdict-source.c:515
#: src/gdict-source.c:540
#, c-format
msgid "Unable to get the “%s” key inside the dictionary source definition: %s"
msgstr ""
"\"%s\" кілтін сөздіктерді ұсынатын қайнар көзінің анықтамасынан алу мүмкін "
"емес: %s"

#: src/gdict-source.c:565
#, c-format
msgid ""
"Unable to get the “%s” key inside the dictionary source definition file: %s"
msgstr ""
"\"%s\" кілтін сөздіктерді ұсынатын қайнар көзінің анықтамасы файлынан алу "
"мүмкін емес: %s"

#: src/gdict-source.c:759
#, c-format
msgid "Dictionary source does not have name"
msgstr "Сөздікті ұсынатын қайнар көзінің аты жоқ"

#: src/gdict-source.c:768
#, c-format
msgid "Dictionary source “%s” has invalid transport “%s”"
msgstr "\"%s\" сөздіктерді ұсынатын қайнар көзінің \"%s\" көлік түрі қате"

#: src/gdict-speller.c:340
msgid "Clear the list of similar words"
msgstr "Ұқсас сөздер тізімін тазарту"

#: src/gdict-strategy-chooser.c:348
msgid "Reload the list of available strategies"
msgstr "Қолжетерлік стратегиялар тізімін қайта жүктеу"

#: src/gdict-strategy-chooser.c:359
msgid "Clear the list of available strategies"
msgstr "Қолжетерлік стратегиялар тізімін тазарту"

#: src/gdict-window.c:410
#, c-format
msgid "No dictionary source available with name “%s”"
msgstr "\"%s\" аты бойынша сөздіктер ұсынатын қайнар көзі табылмады"

#: src/gdict-window.c:414
msgid "Unable to find dictionary source"
msgstr "Сөздіктердің қайнар көзін табу мүмкін емес"

#: src/gdict-window.c:430
#, c-format
msgid "No context available for source “%s”"
msgstr "\"%s\" қайнар көзі үшін контекст қолжетерсіз"

#: src/gdict-window.c:434
msgid "Unable to create a context"
msgstr "Контекстті жасау мүмкін емес"

#: src/gdict-window.c:493
#, c-format
msgid "%s — Dictionary"
msgstr "%s — сөздік"

#: src/gdict-window.c:816
msgid "Save a Copy"
msgstr "Көшірмесін сақтау"

#: src/gdict-window.c:820
msgid "_Save"
msgstr "_Сақтау"

#: src/gdict-window.c:826
msgid "Untitled document"
msgstr "Атаусыз құжат"

#: src/gdict-window.c:847
#, c-format
msgid "Error while writing to “%s”"
msgstr "\"%s\" ішіне жазу қатесі"

#. speller
#: src/gdict-window.c:1216
msgid "Double-click on the word to look up"
msgstr "Сөзді іздеу үшін оның үстіне қос шертіңіз"

#. strat-chooser
#: src/gdict-window.c:1222
msgid "Double-click on the matching strategy to use"
msgstr "Сәйкес келетін стратегияны қолдану үшін оның үстіне қос шертіңіз"

#. source-chooser
#: src/gdict-window.c:1227
msgid "Double-click on the source to use"
msgstr "Қайнар көзді қолдану үшін оның үстіне қос шертіңіз"

#: src/gdict-window.c:1419
msgid "Similar words"
msgstr "Ұқсас сөздер"

#: src/gdict-window.c:1450
msgid "Available strategies"
msgstr "Қолжетерлік стратегиялар"

#: src/gdict-window.c:1466
msgid "Dictionary sources"
msgstr "Қолжетерлік қайнар көздері"

#: src/gdict-utils.c:93
msgid "GDict debugging flags to set"
msgstr "Орнату үшін GDict жөндеу жалаушалары"

#: src/gdict-utils.c:93 src/gdict-utils.c:95
msgid "FLAGS"
msgstr "ЖАЛАУШАЛАР"

#: src/gdict-utils.c:95
msgid "GDict debugging flags to unset"
msgstr "Орнатуды алып тастау үшін GDict жөндеу жалаушалары"

#: src/gdict-utils.c:150
msgid "GDict Options"
msgstr "GDict опциялары"

#: src/gdict-utils.c:151
msgid "Show GDict Options"
msgstr "GDict опцияларын көрсету"

#~ msgid "Default"
#~ msgstr "Default"

#~ msgid "accessories-dictionary"
#~ msgstr "accessories-dictionary"

#~ msgid "spanish"
#~ msgstr "spanish"

#~ msgid "Spanish Dictionaries"
#~ msgstr "Испан сөздіктері"

#~ msgid "Help"
#~ msgstr "Көмек"

#~ msgid "About"
#~ msgstr "Осы туралы"

#~ msgid "_Save a Copy..."
#~ msgstr "Кө_шірмесін сақтау..."

#~ msgid "Available _Dictionaries"
#~ msgstr "Қолжетерлік _сөздіктер"

#~ msgid "_File"
#~ msgstr "_Файл"

#~ msgid "_New"
#~ msgstr "_Жаңа"

#~ msgid "_Copy"
#~ msgstr "_Көшіру"

#~ msgid "Select _All"
#~ msgstr "Барлығын _таңдау"

#~ msgid "Find Pre_vious"
#~ msgstr "А_лдыңғысын табу"

#~ msgid "S_tatusbar"
#~ msgstr "Қал_ып-күй жолағы"

#~ msgid "Port"
#~ msgstr "Порт"

#~ msgid "Status"
#~ msgstr "Қалып-күйі"

#~ msgid "Local Only"
#~ msgstr "Тек жергілікті"

#~ msgid "Not found"
#~ msgstr "Табылмады"

#~ msgid "F_ind:"
#~ msgstr "_Табу:"

#~ msgid "_Previous"
#~ msgstr "А_лдыңғы"

#~ msgid "_Next"
#~ msgstr "_Келесі"

#~ msgid "Filename"
#~ msgstr "Файл аты"

#~ msgid "Name"
#~ msgstr "Аты"

#~ msgid "Description"
#~ msgstr "Сипаттамасы"

#~ msgid "Editable"
#~ msgstr "Түзетуге болады"

#~ msgid "Database"
#~ msgstr "Дерекқор"

#~ msgid "Strategy"
#~ msgstr "Стратегия"

#~ msgid "Context"
#~ msgstr "Контекст"

#~ msgid "Paths"
#~ msgstr "Жолдар"

#~ msgid "Sources"
#~ msgstr "Қайнар көздер"

#~ msgid "Look _up"
#~ msgstr "Іздеп қар_ау"

#~ msgid "Available dictionaries"
#~ msgstr "Қолжетерлік сөздіктер"
